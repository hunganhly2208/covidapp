/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import AppHeader from './src/Navigation/AppHeader';
import { Provider } from 'react-redux'
import store from './src/Store'
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';

import { enableScreens } from 'react-native-screens';
enableScreens()

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />

        <Provider store={store}>

          <SafeAreaView style={{ flex: 1 }}>
            <ApplicationProvider {...eva} theme={eva.light}>
              <AppHeader />
            </ApplicationProvider>
          </SafeAreaView>

        </Provider>

    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
});

export default App;
