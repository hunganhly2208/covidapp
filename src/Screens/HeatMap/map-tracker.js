import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { Marker } from 'react-native-maps';
// import { Ionicons } from '@expo/vector-icons';

const MapTracker = ({coordinate}) => {
  console.log('coordinate tracker', coordinate)
  return (
    <Marker
      coordinate={coordinate}
      title="My location"
    >
      {/* <Ionicons
        name="ios-sunny"
        size={50}
        color={'red'}
      /> */}
    </Marker>
  )
}

export default MapTracker