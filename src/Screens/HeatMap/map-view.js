import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import _MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

import { _HeatMap } from 'react-native-maps'
import { LATIITUDE, LONGIITUDE, LATIITUDE_DELTA, LONGIITUDE_DELTA } from '../../Commons/Constants'
import { selectLoading, getHopikins } from '../../Store/covidSlice'
import ContentMap from './content-map';
import MapTracker from './map-tracker';

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})

// const ZOOM = 8
const ZOOM = 12


const MapView = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectLoading('hopikins'))
  const [coordinate, setCoordinate] = useState({
    latitude: LATIITUDE,
    longitude: LONGIITUDE,
    latitudeDelta: ZOOM,
    longitudeDelta: ZOOM
  })
  const mapView = useRef(null)
  useEffect(() => {
    dispatch(getHopikins)
    getLocation()
  }, [dispatch])


  const getLocation = () => {
    setCoordinate({
      latitude: 10.7229654,
      longitude: 106.6103573,
      latitudeDelta: ZOOM,
      longitudeDelta: ZOOM
    })
    Geolocation.getCurrentPosition(
      ({ coords }) => {
        setCoordinate({
          latitude: parseFloat(coords.latitude),
          longitude: parseFloat(coords.longitude),
          latitudeDelta: ZOOM,
          longitudeDelta: ZOOM
        })
      },
      (error) => alert('Error: Are location services on?'),
      { enableHighAccuracy: true }
    )
  }
  Geolocation.getCurrentPosition(info => console.log(info));
  
  const goToInitLocation = () => {
    const initRegion = Object.assign({}, coordinate)
    // initRegion.latitudeDelta = .5
    // initRegion.longitudeDelta = .5
    initRegion.latitudeDelta = 40
    initRegion.longitudeDelta = 40
    mapView.current.animateToRegion(coordinate, 6000)
  }

  return !isLoading && (
    <View style={{ flex: 1 }}>
      <_MapView
        ref={mapView}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        zoomEnabled={true}
        loadingEnabled
        initialRegion={{
          latitude: LATIITUDE,
          longitude: LONGIITUDE,
          latitudeDelta: 60,
          longitudeDelta: 60
        }}
        showsUserLocation={true}
        onMapReady={goToInitLocation}
      >
        <ContentMap />
        <MapTracker coordinate={coordinate} />
      </_MapView>


    </View>
  )
}

export default MapView