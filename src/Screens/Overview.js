import React, { useCallback, useEffect } from 'react';
import {
    StyleSheet, View, Text, ScrollView, Picker,
    Image,
    Dimensions,
    RefreshControl,
    SafeAreaView,
    ActivityIndicator
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { addnote } from '../Store/Overview/actions'
import { getOverview, selectOverview, selectLoading } from '../Store/covidSlice';
import Card from '../Components/Card';
import { globalStyles } from '../StylesGlobal'

import { Block, Button, TextView } from '../Components';
import { Colors } from '../color';
const W = Dimensions.get('window').width;
// import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';


const styles = StyleSheet.create({
    text: {
        color: 'skyblue',
    },

    content: {
        flex: 1,
    },

    commonHeader: {
        color: '#000',
        padding: 10,
        fontWeight: 'bold',
    },

    titleHeader: {
        borderBottomColor: '#e2d8d8',
        borderBottomWidth: 1,
    },

    text: {
        color: '#000',
        padding: 10,
    },



    img: {
        width: '100%',
        height: 200,
        position: 'absolute',
        top: -30
    },
    doctor: {
        position: 'absolute',
        top: 100,
        left: 60,

        // width: 50,
        // height: 80,
    },
    wrapperimage: {
        position: 'absolute',
        bottom: 0,

        alignSelf: 'center',
        width: W,
        height: 280,
    },
    bg: {
        position: 'absolute',
        width: 1000,
        height: 1000,
        top: -(930 - W / 4),
        alignSelf: 'center',
        // top: 500 - W / 2,
        // left: 500 - W / 2,
        borderRadius: 1000,
        overflow: 'hidden',
    },
    containerHeader: {
        position: 'relative',

    },
    map: {
        borderRadius: 8,
        marginTop: 15,
        padding: 15,
    },

    container: {
        flex: 1,
        justifyContent: "center"
      },
      horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
      }
})



const ItemDot = ({ color1, color2, num, title }) => {
    return (
        <Block block>
            <Block middle>
                <Block
                    width={30}
                    height={30}
                    middle
                    centered
                    borderRadius={30}
                    color={color1}>
                    <Block
                        width={20}
                        height={20}
                        borderWidth={4}
                        borderRadius={20}
                        borderColor={color2}
                    />
                </Block>
                <TextView padding={15} color={color2} h3>
                    {num}
                </TextView>
                <TextView color="gray" h6>
                    {title}
                </TextView>
            </Block>
        </Block>
    );
};

function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}

const Overview = () => {

    const dispatch = useDispatch();
    const overview = useSelector(selectOverview);
    const loading = useSelector(selectLoading('overview'));
    const [refreshing, setRefreshing] = React.useState(false);

    const fetchData = useCallback(() => {
        dispatch(getOverview);
    }, [dispatch]);

    useEffect(() => {
        console.log('overview 1', overview)
        fetchData();
    }, [fetchData]);

    useEffect(() => {
        console.log('overview', overview)
    }, [overview])

    console.log('render overview', overview)

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(0).then(() => {
            setRefreshing(false)
            fetchData();
        });
    }, [refreshing]);

    return (overview && (
        (
            <>
                {
                    loading ? (
                        <View style={[styles.container, styles.horizontal]}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : (
                            <ScrollView
                                style={globalStyles.container}
                                refreshControl={
                                    <RefreshControl refreshing={loading} onRefresh={onRefresh} />
                                }>
                                <View style={{ flex: 1, height: 200 }}>
                                    <Block height={100} color={Colors.blue} style={styles.bg}>
                                        <Block style={styles.wrapperimage}>
                                            <Image
                                                style={styles.doctor}
                                                source={require('../images/Drcorona.png')}
                                            />
                                        </Block>
                                    </Block>
                                    <Block style={styles.containerHeader}>
                                        <Image style={styles.img} source={require('../images/virus.png')} />
                                    </Block>
                                </View>
                                <View style={{ alignItems: 'center', marginBottom: 12 }}>
                                    <Text category="h6">COVID-19 CORONAVIRUS OUTBREAK</Text>
                                    <Text category="s1">{`Last update: ${new Date(
                                        overview.updated,
                                    ).toUTCString()}`}</Text>
                                </View>
                                <View style={{ flex: 2 }}>

                                    <Card
                                        colorBorderTop="#e2d8d8"
                                        marginBottom={15}>
                                        <Text style={[styles.commonHeader, styles.titleHeader]}>Total Confirmed</Text>
                                        <Text style={[styles.commonHeader, styles.text]}>{overview.cases.toLocaleString()}</Text>
                                    </Card>
                                    <Card
                                        colorBorderTop="#ff9903"
                                        marginBottom={15}>
                                        <Text style={[styles.commonHeader, styles.titleHeader]}>Active Cases</Text>
                                        <Text style={[styles.commonHeader, styles.text]}>{overview.active.toLocaleString()}</Text>
                                    </Card>
                                    <Card
                                        colorBorderTop="#0af012"
                                        marginBottom={15}>
                                        <Text style={[styles.commonHeader, styles.titleHeader]}>Total Recovered</Text>
                                        <Text style={[styles.commonHeader, styles.text]}>{overview.recovered.toLocaleString()}</Text>
                                    </Card>
                                    <Card
                                        colorBorderTop="#fd1212"
                                        marginBottom={15}>
                                        <Text style={[styles.commonHeader, styles.titleHeader]}>Total Deaths</Text>
                                        <Text style={[styles.commonHeader, styles.text]}>{overview.deaths.toLocaleString()}</Text>
                                    </Card>
                                </View>
                            </ScrollView>
                        )
                }

            </>
        )
    ))
}

export default Overview