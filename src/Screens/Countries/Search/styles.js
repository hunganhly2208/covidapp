import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    input: {
        borderColor: '#e2d8d8',
        borderRadius: 6,
        borderWidth: 1,
        padding: 10,
        width: '100%',
        marginRight: 10,
    },

    picker: {
        // borderRadius: 6,
        // padding: 5,
        // flex: 1
    },

    content: {
        flexDirection: 'row',
    },
    text: {
        color: '#2dcce0',
    },

    title: {
        color: '#fff',
        paddingVertical: 10,
        paddingHorizontal: 5,
    },

    blockSearch: {
        alignItems: 'stretch',
        flexDirection: 'row',
        height: 32,
        backgroundColor: '#F5FCFF',
    },

    blockResultSearch: {
       
    },

    rowRecord: {
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
    }
})

